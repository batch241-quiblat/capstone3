import { useState, useEffect, useContext } from 'react';

import { Card, Button, ListGroup } from 'react-bootstrap';

import { Link, Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function ProductCard({product}) {

  const { user } = useContext(UserContext);

  const [availability, setAvailability] = useState('');

  const {name, description, category, price, isAvailable, _id} = product;
  
  function deactivate(e){
    fetch(`${process.env.REACT_APP_API_URL}/products/archiveProduct/${_id}`,{
      method: "PATCH",
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        isAvailable: false
      })
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);
        if(data.success){
            Swal.fire({
              title: "Success!",
              icon: "success",
              text: "Product is now Inactive."
            });
        } else {

              Swal.fire({
              title: "Error!",
              icon: "error",
              text: "Please try again."
            });
        }      
    });

  }

  function activate(e){
    fetch(`${process.env.REACT_APP_API_URL}/products/archiveProduct/${_id}`,{
      method: "PATCH",
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        isAvailable: true
      })
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);
        if(data.success){
            Swal.fire({
              title: "Success!",
              icon: "success",
              text: "Product is now Active."
            });
        } else {

              Swal.fire({
              title: "Error!",
              icon: "error",
              text: "Please try again."
            });
        }      
    });

  }

  useEffect(() => {
      if(isAvailable === false){
        setAvailability("Inactive")
      } else {
        setAvailability("Active")
      }
    }, [availability,isAvailable])

  return (
    (user.id === null) ?
    <Navigate to="/login"/>
    :
    <Card className="m-2 p-2" style={{ width: '18rem' }}>
      <Card.Img variant="top" src="https://img.freepik.com/free-photo/portrait-adorable-domestic-cat_23-2149167104.jpg?size=626&ext=jpg&ga=GA1.2.503283607.1676868923&semt=sph cap" />
      <Card.Body className="pb-0">
        <Card.Title className="m-0 text-center text-capitalize">{name}</Card.Title>
      </Card.Body>
      <ListGroup className="list-group-flush border-top-0">
        <ListGroup.Item ><span className="h6">Description: </span>{description}</ListGroup.Item>
        { (user.isAdmin === true) ?
            <ListGroup.Item ><span className="h6 text-capitalize">Status: </span>{availability}</ListGroup.Item>
          :
            null
        }
        <ListGroup.Item className="text-capitalize" ><span className="h6">Category: </span>{category}</ListGroup.Item>
        <ListGroup.Item className="" ><span className="h6">Price: </span>{price}</ListGroup.Item>
      </ListGroup>
      
      <Card.Body className="m-auto">
      { (user.isAdmin === true) ?
        <>
          <Button className="bg-primary m-2" as={Link} to={`/products/${_id}`}>Edit</Button>
          { (isAvailable) ? 
          
              <Button className="bg-danger m-2 border-0" onClick={(e) => deactivate(e)}>Deactivate</Button>
            :
              <Button className="bg-success m-2 border-0" onClick={(e) => activate(e)}>Activate</Button>
          }
        </>
        :
        <>
          <Button className="bg-primary m-2 border-0" as={Link} to={"/allActiveProducts"}>Add Cart</Button>
          <Button className="bg-danger m-2 border-0" as={Link} to={`/buyNow/${_id}`}>Buy Now</Button>
        </>
      }
        
      </Card.Body>
    </Card>
  )
}
