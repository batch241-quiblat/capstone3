import { useContext } from 'react';

import {Link, NavLink} from 'react-router-dom';

import UserContext from '../UserContext';

import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';

export default function AppNavbar() {

    const { user } = useContext(UserContext);
    console.log(user.firstName);

  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to="/">
          <img
              alt=""
              src="../s.png"
              width="30"
              height="30"
              className="d-inline-block align-top rounded-pill"
            />{''}
            hane
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">

            { (user.id !== null) ?
              (user.isAdmin === true) ?
                <>
                  <Nav.Link as={NavLink} to="/allProducts">All Products</Nav.Link>
                  <Nav.Link as={NavLink} to="/allActiveProducts">Active Products</Nav.Link>
                  <Nav.Link as={NavLink} to="/allInactiveProducts">Inactive Products</Nav.Link>
                  <Nav.Link as={NavLink} to="/addProduct">Add Product</Nav.Link>
                  <NavDropdown className="text-capitalize" title={user.firstName} id="basic-nav-dropdown">
                  <NavDropdown.Item href="#action/3.1">Profile</NavDropdown.Item>
                  <NavDropdown.Item href="#action/3.3">Change Password</NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item as={NavLink} to="/logout">
                    Logout
                  </NavDropdown.Item>
                  </NavDropdown>
                </>

                :

                <>
                  <Nav.Link as={NavLink} to="/allActiveProducts">Products</Nav.Link>
                  <Nav.Link as={NavLink} to="/orders">Orders</Nav.Link>
                  <NavDropdown className="text-capitalize" title={user.firstName} id="basic-nav-dropdown">
                  <NavDropdown.Item href="#action/3.1">Profile</NavDropdown.Item>
                  <NavDropdown.Item href="#action/3.3">Change Password</NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item as={NavLink} to="/logout">
                    Logout
                  </NavDropdown.Item>
                  </NavDropdown>
                </>
              :
              <>
                <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
              </>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}


