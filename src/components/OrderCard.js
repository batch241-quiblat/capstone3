import { useState, useEffect, useContext } from 'react';

import { Card } from 'react-bootstrap';

import { Navigate } from 'react-router-dom';

import { format } from 'date-fns'

import UserContext from '../UserContext';


export default function OrderCard({order}) {

  const { user } = useContext(UserContext);

  const {products, totalAmount, purchasedOn} = order;

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [category, setCategory] = useState("");
  const [price, setPrice] = useState(0); 

  useEffect(() => {

    products.forEach(function(prod){
      fetch(`${process.env.REACT_APP_API_URL}/products/${prod.productId}`,{
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(res => res.json())
      .then(data => {
          console.log(data);
          setName(data.name);
          setCategory(data.category);
          setDescription(data.description);
          setPrice(data.price);
      });
    })
  })

  return (
    (user.id === null) ?
    <Navigate to="/login"/>
    :
    <Card className="m-2 p-0" style={{ width: '50rem', height: '15rem' }}>
      <div className="d-flex w-100 h-100">
        <div className="w-25 m-2">
          <Card.Img className="imgs" src="https://img.freepik.com/free-photo/portrait-adorable-domestic-cat_23-2149167104.jpg?size=626&ext=jpg&ga=GA1.2.503283607.1676868923&semt=sph cap" />
        </div>
        <div className="container w-75 h-auto">
          <Card.Body>
            <Card.Title className="text-center text-capitalize h2">{name}</Card.Title>
            <Card.Text className="text-center" >{description}</Card.Text>
          </Card.Body>
          <div className="container d-flex justify-content-around">
            <div className="list-group-flush">
              <Card.Text className="" ><span className="h5 text-capitalize">Category: </span>{category}</Card.Text>
              <Card.Text className="" ><span className="h5">Price: </span>{price}</Card.Text>
            </div>
            <div className="list-group-flush">
              <Card.Text className="" ><span className="h5">Quantity: </span>{products[0].quantity}</Card.Text>
              <Card.Text className="" ><span className="h5">Total: </span>{totalAmount}</Card.Text>
            </div>
          </div>
          <div className="list-group-flush">
              <Card.Text className="bg-success py-1 px-2 text-white mt-4 mx-5 text-center rounded-pill" >{format(new Date(purchasedOn), 'MMMM dd, yyyy')}</Card.Text>
            </div>
        </div>
      </div>
    </Card>
  )
}
