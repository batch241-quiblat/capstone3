import { useState, useEffect, useContext } from 'react';

import { Card, Button, ListGroup } from 'react-bootstrap';

import { useParams, Link, useNavigate, Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function ProductView() {

  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const {productId} = useParams();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [category, setCategory] = useState("");
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(1);


  const checkout = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/addOrder/${productId}`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data.success){
				Swal.fire({
				  title: data.success,
				  icon: "success",
				  text: "You have successfully enrolled for this course."
				});
				navigate("/allActiveProducts");
			} else {
				 Swal.fire({
				  title: data.error,
				  icon: "error",
				  text: "Please try again."
				});
			}
		});
	}

	useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`,{
	      method: "GET",
	      headers: {
	        'Content-Type': 'application/json',
	        Authorization: `Bearer ${localStorage.getItem('token')}`
	      }
	    })
	    .then(res => res.json())
	    .then(data => {
	        console.log(data);
	        setName(data.name);
	        setCategory(data.category);
	        setDescription(data.description);
	        setPrice(data.price);
	    });
    }, [productId])

  return (
  	(user.id === null) ?
    <Navigate to="/login"/>
    :
  	<div className="container">
        <div className="row justify-content-center">
			<Card className="m-2 pt-2" style={{ width: '25rem' }}>
			  <Card.Img variant="top" src="https://img.freepik.com/free-photo/portrait-adorable-domestic-cat_23-2149167104.jpg?size=626&ext=jpg&ga=GA1.2.503283607.1676868923&semt=sph cap" />
			  <Card.Body>
			    <Card.Title className="text-center text-capitalize mb-0">{name}</Card.Title>
			  </Card.Body>
			  <ListGroup className="list-group-flush">
			  	<ListGroup.Item ><span className="h6">Description: </span>{description}</ListGroup.Item>
			    <ListGroup.Item className="text-capitalize"><span className="h6 text-capitalize">Category: </span>{category}</ListGroup.Item>
			    <ListGroup.Item ><span className="h6">Price: </span>{price}</ListGroup.Item>
			    
			  </ListGroup>
			  <Card.Body className="py-0">
			  <Card.Text className="my-1 h6 mt-3">Quantity</Card.Text>
			  <div className="d-flex">
			  	<span><Button variant="success" onClick={() => {if(quantity === 1){setQuantity(1)}else{setQuantity(quantity - 1)}}} className="px-3 rounded-0 h-75 d-flex align-items-center">-</Button></span>
			  	<span className=""><Card.Text className="border border-secondary h-75 px-4 d-flex align-items-center">{quantity}</Card.Text></span>
			  	<span><Button variant="success" onClick={() => setQuantity(quantity + 1)} className="px-3 rounded-0 h-75 d-flex align-items-center">+</Button></span>
			  </div>
			  </Card.Body>
			  <Card.Body className="m-auto">
			    <Button className="bg-primary m-2 mt-0" onClick={() => checkout(productId)} as={Link} to={"/allActiveProducts"}>Checkout</Button>
			  </Card.Body>
			</Card>
		</div>
    </div>
  )
}
