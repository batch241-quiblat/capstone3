import { useState, useEffect, useContext } from 'react';

import { useNavigate, Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

import { Form, Button } from 'react-bootstrap';

import Swal from 'sweetalert2';

export default function UpdateProduct() {

	const { user } = useContext(UserContext);

    const navigate = useNavigate();

    const [name, setName] = useState('');
    const [category, setCategory] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');

    const [isActive, setIsActive] = useState(false);


    function add(e) {

        e.preventDefault();

            fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                     Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    name: name,
                    description: description,
                    category: category,
                    price: price,
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                if(data.success){
                    Swal.fire({
                      title: data.success,
                      icon: "success",
                      text: ""
                    });
                    setName('');
                    setCategory('');
                    setDescription('');
                    setPrice('');
                    navigate("/allProducts");
                } else {
                    Swal.fire({
                        title: data.error,
                        icon: "error",
                        text: "Please try again."
                    });
                }
                
            })
        
    }


    useEffect(() => {
    	

        // Validation to enable submit button when all fields are populated and both passwords match
        if(name !== '' && category !== '' && description !== '' && price !== ''){
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    
    }, [name, category, description, price]);
	
	return (
        (user.id === null) ?
        <Navigate to="/login"/>
        :
		<div className="container">
            <div className="row justify-content-center">
                <div className="col-6 mt-5 px-5 shadow p-3 mb-5 bg-body rounded">
                    <Form onSubmit={(e) => add(e)}>
                        <h1 className="mb-3 text-center">Add Product</h1>
                        <Form.Group className="mb-3" controlId="name">
                            <Form.Label className="mb-0 mb-0">Product Name</Form.Label>
                            <Form.Control 
                                type="text" 
                               
                                value={name}
                                onChange={e => setName(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="category">
                            <Form.Label className="mb-0 mb-0" >Category</Form.Label>
                            <Form.Control 
                                type="text" 
                               
                                value={category}
                                onChange={e => setCategory(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="description">
                            <Form.Label className="mb-0 mb-0" >Description</Form.Label>
                            <Form.Control 
                                type="yexy" 
                                
                                value={description}
                                onChange={e => setDescription(e.target.value)} 
                                required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="price">
                            <Form.Label className="mb-0 mb-0" >Price</Form.Label>
                            <Form.Control 
                                type="text" 
                   
                                value={price}
                                onChange={e => setPrice(e.target.value)}
                                required
                            />
                        </Form.Group>

                        {/*conditionally render submit button based on "isActive" state*/}
                        { isActive ?
                            <div className="text-center">
                                <Button variant="success" type="submit" id="submitBtn">Submit</Button>
                            </div>
                        :
                            <div className="text-center">
                                <Button variant="success" type="submit" id="submitBtn" disabled>Submit</Button>
                            </div>
                        }

                    </Form>
                </div>
            </div>
        </div>

	)
}