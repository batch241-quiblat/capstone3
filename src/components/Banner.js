import { Button, Row, Col } from 'react-bootstrap';
import { useContext } from 'react';

import { Link } from 'react-router-dom';

import UserContext from '../UserContext';

export default function Banner() {

    const { user } = useContext(UserContext);

    const admins = {
        title: "Welcome Admin to Shane's Online Shop!",
        content: "Add Now, Add Anywhere!",
        destination: "/allProducts",
        label: "Shop Now"
    }

    const users = {
        title: "Welcome to Shane's Online Shop!",
        content: "Shop Now, Shop Anywhere!",
        destination: "/allActiveProducts",
        label: "Shop Now"
    }




    return (
        (user.id === null) ?
        <Row>
                <Col className="p-5">
                    <h1>{users.title}</h1>
                    <p>{users.content}</p>
                    <Button variant="primary" as={Link} to={"/login"}>Login Now</Button>           
                </Col>
            </Row>
        :
        (user.isAdmin === true)?
            <Row>
                <Col className="p-5">
                    <h1>{admins.title}</h1>
                    <p>{admins.content}</p>
                    <Button variant="primary" as={Link} to={admins.destination}>Show Products</Button>  
                      
                </Col>
            </Row>
            :
            <Row>
                <Col className="p-5">
                    <h1>{users.title}</h1>
                    <p>{users.content}</p>
                    <Button variant="primary" as={Link} to={users.destination}>Shop Now</Button>           
                </Col>
            </Row>
        
    )
}