import './App.css';

import { useState, useEffect } from 'react';

import { UserProvider } from './UserContext';

import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

import AppNavbar from './components/AppNavbar';

import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import AllProducts from './pages/AllProducts';
import AllActiveProducts from './pages/AllActiveProducts';
import AllInactiveProducts from './pages/AllInactiveProducts';
import Orders from './pages/Orders';
import Logout from './pages/Logout';
import Error from './pages/Error';
import UpdateProduct from './components/UpdateProduct';
import ProductView from './components/ProductView';
import AddProduct from './components/AddProduct';

function App() {

  const [ user, setUser ] = useState({
    // Allows us to store and to access the properties in the "user ID" and "isAdmin" data.
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      // User is loggen in
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          firstName: data.firstName
        });
        // User is logged out
      } else {
        setUser({
          id: null,
          isAdmin: null,
          firstName: null
        });
      }
    });
  }, []);

  return (
    
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <Routes>
          <Route path="/" element={<Home/>}/>
          <Route path="/login" element={<Login/>}/>
          <Route path="/register" element={<Register/>}/>
          <Route path="/allProducts" element={<AllProducts/>}/>
          <Route path="/allActiveProducts" element={<AllActiveProducts/>}/>
          <Route path="/allInactiveProducts" element={<AllInactiveProducts/>}/>
          <Route path="/orders" element={<Orders/>}/>
          <Route path="/products/:productId" element={<UpdateProduct/>}/>
          <Route path="/buyNow/:productId" element={<ProductView/>}/>
          <Route path="/addProduct" element={<AddProduct/>}/>
          <Route path="/logout" element={<Logout/>} />
          <Route path="/*" element={<Error/>} />
        </Routes>
      </Router>
    </UserProvider>
    </>
  )
}

export default App;
