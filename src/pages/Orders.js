import { useState, useEffect, useContext } from 'react';

import OrderCard from '../components/OrderCard';
import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

export default function Orders(){

	const { user } = useContext(UserContext);

	// State that will be used to store the courses retrieved from the database
	const [orders, setOrder] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/myOrders`,{
			headers: {
		    	Authorization: `Bearer ${localStorage.getItem('token')}`
		    }
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setOrder(data.map(order => {
				return (
					<OrderCard key={order.id} order ={order} />
				);
			}));
		});
	}, []);
	return (
		(user.id === null) ?
        <Navigate to="/login"/>
        :
		<>
		<div className="mx-5">
			<div className="row justify-content-center mt-2">
				{orders}
			</div>	
		</div>
		</>
	)
}