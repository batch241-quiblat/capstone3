import { useState, useEffect, useContext } from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function Login(){


    const { user, setUser } = useContext(UserContext);


    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('')
    const [isActive, setIsActive] = useState(false);


    function authenticate(e){
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            if(data.access){
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                  title: "Welcome Back!",
                  icon: "success",
                  text: "You are now successfully login."
                });

                setEmail('');
                setPassword('');

            } else {

                  Swal.fire({
                  title: data.error,
                  icon: "error",
                  text: "Please try again."
                });

                if(data.error === "User does not exists."){
                    setEmail('');
                    setPassword('');
                } else {
                    setPassword('');
                }
            }      
        });
    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setUser({
                id: data._id,
                isAdmin: data.isAdmin,
                firstName: data.firstName
            });
        });
    }

    useEffect(() => {
        if(email !== '' && password !==''){
            setIsActive(true);
        } else{
            setIsActive(false);
        }
    }, [email, password])

    return (

        (user.id !== null) ?
        <Navigate to="/"/>
        :
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-6 mt-5 px-5 shadow p-3 mb-5 bg-body rounded">
                    <Form onSubmit={(e) => authenticate(e)}>
                        <h1 className="mb-3 text-center">Login</h1>
                        <Form.Group className="mb-2" controlId="userEmail">
                            <Form.Label className="mb-0 mb-0" >Email address</Form.Label>
                            <Form.Control 
                                type="email" 
                                placeholder="Enter email" 
                                value={email}
                                onChange={e => setEmail(e.target.value)}
                                required
                            />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="password1">
                            <Form.Label className="mb-0 mb-0" >Password</Form.Label>
                            <Form.Control 
                                type="password" 
                                placeholder="Password" 
                                value={password}
                                onChange={e => setPassword(e.target.value)}
                                required
                            />
                        </Form.Group>

                         { isActive ?
                            <div className="text-center">
                                <Button variant="success" type="submit" id="submitBtn">Submit</Button>
                            </div>
                        :
                            <div className="text-center">
                                <Button variant="success" type="submit" id="submitBtn" disabled>Submit</Button>
                            </div>
                        }
                    </Form>
                </div>
            </div>
        </div>
    )
}