import Banner from '../components/Banner';
import Container from 'react-bootstrap/Container';

export default function Home() {

    return (
        <>  
        <Container>
            <Banner/>
        </Container>  
        </>
    )
}
