import { useState, useEffect, useContext } from 'react';

import { Navigate } from 'react-router-dom';

import ProductCard from '../components/ProductCard';

import UserContext from '../UserContext';

export default function AllActiveProduct(){

	const { user } = useContext(UserContext);

	// State that will be used to store the courses retrieved from the database
	const [products, setProduct] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/allActive`,{
			headers: {
		    	Authorization: `Bearer ${localStorage.getItem('token')}`
		    }
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProduct(data.map(product => {
				return (
					<ProductCard key={product.id} product ={product} />
				);
			}));
		});
	});
	return (
		(user.id === null) ?
        <Navigate to="/login"/>
        :
		<>
		<div className="mx-5">
			{ (user.isAdmin === true ) ?
				<h2 className="text-center mt-2">Active Products</h2>
				:
				<h2 className="text-center mt-2">Products</h2>
			}
			<div className="row justify-content-center mt-2">
				{products}
			</div>	
		</div>
		</>
	)
}