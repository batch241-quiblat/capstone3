import { useState, useEffect, useContext } from 'react';

import UserContext from '../UserContext';

import { Navigate, useNavigate } from 'react-router-dom';

import { Form, Button } from 'react-bootstrap';

import Swal from 'sweetalert2';

export default function Register() {


    const { user } = useContext(UserContext);

    const navigate = useNavigate();


    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    const [isActive, setIsActive] = useState(false);
    const [itExists, setItExists] = useState(true);


    function registerUser(e) {

        e.preventDefault();

        if(!itExists){
            fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    firstName: firstName,
                    lastName: lastName,
                    mobileNo: mobileNo,
                    email: email,
                    password: password1
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                if(data.success){
                    Swal.fire({
                      title: data.success,
                      icon: "success",
                      text: "Login Now!"
                    });
                    setFirstName('');
                    setLastName('');
                    setMobileNo('');
                    setEmail('');
                    setPassword1('');
                    setPassword2('');
                    navigate("/login");
                } else {
                    Swal.fire({
                        title: data.error,
                        icon: "error",
                        text: "Please try again."
                    });
                }
                
            })
        } else {
            Swal.fire({
                title: "Email already exists",
                icon: "error",
                text: "Please provide a different email."
            });
            setEmail('');
        }
    }

    useEffect(() => {

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email,
                })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                setItExists(true);
            } else {
                setItExists(false);
            }
        })

        // Validation to enable submit button when all fields are populated and both passwords match
        if((email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !=='' && mobileNo !=='') && (password1 === password2) && mobileNo.length === 11){
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    
    }, [email, password1, password2, firstName, lastName, mobileNo]);

    return (
        (user.id !== null) ?
        <Navigate to="/"/>
        :
         <div className="container">
            <div className="row justify-content-center">
                <div className="col-6 mt-5 px-5 shadow p-3 mb-5 bg-body rounded">
                    <Form onSubmit={(e) => registerUser(e)}>
                        <h1 className="mb-3 text-center">Register</h1>
                        <Form.Group className="mb-3" controlId="firstName">
                            <Form.Label className="mb-0 mb-0">First Name</Form.Label>
                            <Form.Control 
                                type="text" 
                                placeholder="First Name" 
                                value={firstName}
                                onChange={e => setFirstName(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="lastName">
                            <Form.Label className="mb-0 mb-0" >Last Name</Form.Label>
                            <Form.Control 
                                type="text" 
                                placeholder="Last Name" 
                                value={lastName}
                                onChange={e => setLastName(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="userEmail">
                            <Form.Label className="mb-0 mb-0" >Email address</Form.Label>
                            <Form.Control 
                                type="email" 
                                placeholder="Enter email"
                                value={email}
                                onChange={e => setEmail(e.target.value)} 
                                required
                            />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="mobileNo">
                            <Form.Label className="mb-0 mb-0" >Mobile Number</Form.Label>
                            <Form.Control 
                                type="text" 
                                placeholder="Mobile Number" 
                                value={mobileNo}
                                onChange={e => setMobileNo(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="password1">
                            <Form.Label className="mb-0 mb-0" >Password</Form.Label>
                            <Form.Control 
                                type="password" 
                                placeholder="Password" 
                                value={password1}
                                onChange={e => setPassword1(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="password2">
                            <Form.Label className="mb-0 mb-0" >Verify Password</Form.Label>
                            <Form.Control 
                                type="password" 
                                placeholder="Verify Password" 
                                value={password2}
                                onChange={e => setPassword2(e.target.value)}
                                required
                            />
                        </Form.Group>
                        {/*conditionally render submit button based on "isActive" state*/}
                        { isActive ?
                            <div className="text-center">
                                <Button variant="success" type="submit" id="submitBtn">Submit</Button>
                            </div>
                        :
                            <div className="text-center">
                                <Button variant="success" type="submit" id="submitBtn" disabled>Submit</Button>
                            </div>
                        }

                    </Form>
                </div>
            </div>
        </div>  
    )
}