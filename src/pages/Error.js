import { Button, Row, Col } from 'react-bootstrap';

import { Link } from 'react-router-dom';

export default function Error() {

    const error = {
        title: "Error 404 - Page Not Found!",
        content: "The page you are looking cannot be found.",
        destination: "/",
        label: "Back to Home"
    }




    return (
        <Row>
            <Col className="p-5">
                <h1>{error.title}</h1>
                <p>{error.content}</p>
                <Button variant="primary" as={Link} to={error.destination}>{error.label}</Button>           
            </Col>
        </Row>
    )
}